﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;

namespace EY.Task1
{
    class Program
    {
        static string latinAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        static string russianAlphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя";

        static DirectoryInfo dir = new DirectoryInfo(@"\files");

        static int countOfDeletedLines = 0;

        static string RandomDay(Random gen)
        {
            DateTime start = new DateTime(DateTime.Today.Year - 5, DateTime.Today.Month, DateTime.Today.Day);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(gen.Next(range)).ToShortDateString();
        }

        static string RandomLatinWord(Random gen)
        {
            StringBuilder word = new StringBuilder();
            for (int i = 0; i < 10; i++)
            {
                word.Append(latinAlphabet[gen.Next(0, 52)]);
            }
            return word.ToString();
        }

        static int RandomEvenNaturalNumber(Random gen)
        {
            return gen.Next(1, 50000001) * 2;
        }

        static double RandomDouble(Random gen)
        {
            var next = gen.NextDouble();

            return 1 + (next * (20 - 1));
        }

        static string RandomRussianWord(Random gen)
        {
            StringBuilder word = new StringBuilder();
            for (int i = 0; i < 10; i++)
            {
                word.Append(russianAlphabet[gen.Next(0, 66)]);
            }
            return word.ToString();
        }

        static string GenerateString(Random gen)
        {

            string randomData = RandomDay(gen);
            string randomLatinWord = RandomLatinWord(gen);
            string randomRussianWord = RandomRussianWord(gen);
            string randomEvenNaturalNumber = RandomEvenNaturalNumber(gen).ToString();
            double number = RandomDouble(gen);
            string randomDouble = String.Format("{0:F8}", number);
            string str = (randomData + "||" + randomLatinWord + "||" + randomRussianWord + 
                "||" + randomEvenNaturalNumber + "||" + randomDouble + "||");
            return str;
        }

        static void GenerateSingleFile(object state)
        {    
            using (StreamWriter sw = new StreamWriter((String)state))
            {
                Random gen = new Random();
                for (int k = 0; k < 100000; k++)
                {
                    sw.WriteLine(GenerateString(gen));
                }
            }
            Console.WriteLine(state + ".txt Был успешно создан");
        }

        static void CreateFiles(int filesCount)
        {
            dir.Create();
            Task[] tasks = new Task[filesCount];
            for (int i = 1; i <= filesCount; i++)
            {
                object state = String.Concat(dir.FullName, "\\", i, ".txt");
                tasks[i-1] = new Task(GenerateSingleFile, state);
                tasks[i - 1].Start();
            }
            Task.WaitAll(tasks);
        }

        private static void CombineMultipleFilesIntoSingleFile(string inputDirectoryPath, string inputFileNamePattern, 
            string outputFilePath, string characters)
        {
            countOfDeletedLines = 0;
            File.Delete(outputFilePath);
            string[] inputFilePaths = Directory.GetFiles(inputDirectoryPath, inputFileNamePattern);
            Console.WriteLine("Number of files: {0}.", inputFilePaths.Length);
            using (var outputStream = File.Create(outputFilePath))
            {
                foreach (var inputFilePath in inputFilePaths)
                {
                    using (var inputStream = File.Open(inputFilePath,FileMode.Open))
                    {   
                        inputStream.CopyTo(outputStream);
                        string line = null;
                        inputStream.Position = 0;
                        using (var sr = new StreamReader(inputStream))
                        {
                            using (var sw = new StreamWriter(File.Create(inputDirectoryPath + "\\temp.txt")))
                            {
                                while (!sr.EndOfStream)
                                {
                                    line = sr.ReadLine();
                                    if (!line.Contains(characters))
                                    {
                                        sw.WriteLine(line);
                                    }
                                    else
                                    {
                                        ++countOfDeletedLines;
                                    }
                                }
                            }
                        }
                        File.Delete(inputFilePath);
                        File.Move(inputDirectoryPath + "\\temp.txt", inputFilePath);
                    }
                    Console.WriteLine("Файл {0} был успешно обработан.", inputFilePath);
                }
            }
        }

        public static void ImportFilesIntoDb(string inputDirectoryPath, string inputFileNamePattern)
        {

            var inputFilePaths = Directory.GetFiles(inputDirectoryPath, inputFileNamePattern);
            Console.WriteLine("Number of files: {0}.", inputFilePaths.Length);
            foreach (var inputFilePath in inputFilePaths)
            {
                if (!inputFilePath.Contains("combine"))
                {
                    using (var inputStream = File.Open(inputFilePath, FileMode.Open))
                    {
                        string line = null;
                        SqlConnection connection = DbWorker.GetLocalDB("Lines");
                        SqlCommand cmd = DbWorker.ImportRandomStrings(connection);
                        DbWorker.CreateTable(connection);
                        using (var sr = new StreamReader(inputStream))
                        {
                            while (!sr.EndOfStream)
                            {
                                line = sr.ReadLine();
                                DbWorker.ImportLine(line, cmd);
                            }
                        }
                    }
                }                
            }
        }

        static void Main(string[] args)
        {
            int filesCount = 100;
            CreateFiles(filesCount);
            Console.WriteLine("Выберите номер операции: " + Environment.NewLine +
                "1 - Объединить файлы" + Environment.NewLine +
                "2 - Импорт файлов в БД" + Environment.NewLine +
                "3 - Создать хранимую процедуру для подсчета суммы всех целых чисел и медианы дробных"+ Environment.NewLine +
                "0 - Выход из программы");
            while (true)
            {
                int choice = Int32.Parse(Console.ReadLine());
                if (choice == 1)
                {
                    Console.WriteLine("Введите желаемую последовательность символов для удаления строки");
                    string seqOfSymbols = Console.ReadLine();
                    CombineMultipleFilesIntoSingleFile(dir.FullName, "*.txt", dir + "\\combine.txt", seqOfSymbols);
                    Console.WriteLine("Количество удаленных строк" + countOfDeletedLines);
                }
                else if (choice == 2)
                {
                    ImportFilesIntoDb(dir.FullName, @"*.txt");
                }
                else if (choice == 3)
                {
                    DbWorker.StoredProcedureSumAndMedian();
                }
                else
                {
                    Console.WriteLine("Спасибо за работу!");
                    Environment.Exit(0);
                }
            }
        }
    }
}
