﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace EY.Task1
{
    class DbWorker
    {
        public const string DB_DIRECTORY = "Data";
        public static int counter;

        public static string randomDatePattern = @"((0[1-9])|([1-3][0-2]).(0[1-9])|(1[0-2]).201[3-8])";
        public static string randomLatinWordPattern = @"[a-zA-Z]{10}";
        public static string randomRussianWordPattern = @"\p{IsCyrillic}{10}";
        public static string randomNumberPattern = @"\|\|[1-9][0-9]{0,8}\|\|";
        public static string RandomDoubleNumberPattern = @"[1-9][0-9]?\,[0-9]{8}";


        public static SqlConnection GetLocalDB(string dbName, bool deleteIfExists = false)
        {
            string outputFolder = Path.Combine("C:\\files\\", DB_DIRECTORY);
            string mdfFilename = dbName + ".mdf";
            string dbFileName = Path.Combine(outputFolder, mdfFilename);
            string logFileName = Path.Combine(outputFolder, String.Format("{0}_log.ldf", dbName));

            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);
            }

            if (File.Exists(dbFileName) && deleteIfExists)
            {
                if (File.Exists(logFileName)) File.Delete(logFileName);
                File.Delete(dbFileName);
                CreateDatabase(dbName, dbFileName);
            }

            else if (!File.Exists(dbFileName))
            {
                CreateDatabase(dbName, dbFileName);
            }


            string connectionString = String.Format(@"Data Source=(localdb)\MSSQLLocalDB;AttachDBFileName={1};
                Initial Catalog={0};Integrated Security=SSPI;", dbName, dbFileName);
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        public static void CreateTable(SqlConnection connection)
        {
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "DROP TABLE IF EXISTS RandomLines " +
                "CREATE TABLE RandomLines(ID INT NOT NULL PRIMARY KEY, RandomDate DATE, " +
                "RandomLatinWord CHAR(10), RandomRussianWord NCHAR(10), RandomNumber INT, RandomDoubleNumber float)";
            cmd.ExecuteNonQuery();
        }

        public static SqlCommand ImportRandomStrings(SqlConnection connection)
        {
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = $"INSERT INTO RandomLines(ID, RandomDate, RandomLatinWord, " +
                        $"RandomRussianWord, RandomNumber, RandomDoubleNumber) " +
                        $"VALUES(@counter, CONVERT(date, @randomDate ,104), @randomLatinWord," +
                        $"@randomRussianWord, @randomNumberEdited, @randomDoubleNumber)";
            return cmd;

        }

        public static void ImportLine(string readingLine, SqlCommand cmd)
        {
            counter++;
            Console.WriteLine("Строка №{0} была занесена в базу, осталось {1}", counter, 100000*100 - counter);
            Regex r = new Regex(randomDatePattern);
            Match m = r.Match(readingLine);
            string randomDate = readingLine.Substring(0, 10);

            r = new Regex(randomLatinWordPattern);
            m = r.Match(readingLine);
            string randomLatinWord = m.Value;

            r = new Regex(randomRussianWordPattern);
            m = r.Match(readingLine);
            string randomRussianWord = m.Value;

            r = new Regex(randomNumberPattern);
            m = r.Match(readingLine);
            string randomNumber = m.Value;
            int randomNumberEdited = Int32.Parse(randomNumber.Trim(' ', '|'));

            r = new Regex(RandomDoubleNumberPattern);
            m = r.Match(readingLine);
            double randomDoubleNumber = Double.Parse(m.Value);
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@counter", counter);
            DateTime date = Convert.ToDateTime(randomDate, new CultureInfo("de-DE"));
            cmd.Parameters.AddWithValue("@randomDate", date);
            cmd.Parameters.AddWithValue("@randomLatinWord", randomLatinWord);
            cmd.Parameters.AddWithValue("@randomRussianWord", randomRussianWord);
            cmd.Parameters.AddWithValue("@randomNumberEdited", randomNumberEdited);
            cmd.Parameters.AddWithValue("@randomDoubleNumber", randomDoubleNumber);
            cmd.ExecuteNonQuery();
        }

        public static void StoredProcedureSumAndMedian()
        {
            SqlConnection connection = GetLocalDB("Lines");
            string sp = "CREATE OR ALTER PROCEDURE [sp_SumAndMedian] AS " +
                "SELECT distinct " +
                "PERCENTILE_CONT(0.5) " +
                "WITHIN GROUP(ORDER BY RandomLines.RandomDoubleNumber) " +
                "OVER() AS Median, SUM(CONVERT(decimal,RandomNumber)) OVER() AS SumOfNumbers " +
                "FROM [RandomLines] ";
            using (SqlCommand createCommand = new SqlCommand(sp, connection))
            {
                createCommand.CommandType = CommandType.Text;
                createCommand.ExecuteNonQuery();
            }

            using (SqlCommand sumCommand = new SqlCommand("sp_SumAndMedian", connection))
            {
                sumCommand.CommandType = CommandType.StoredProcedure;

                var sum = sumCommand.ExecuteReader();
                while (sum.Read())
                {
                    Console.WriteLine("Sum is - " + sum.GetDecimal(1) + " median is  - " + sum.GetDouble(0));
                }
            }
        }

        public static bool CreateDatabase(string dbName, string dbFileName)
        {
            string connectionString = String.Format(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=SSPI;");
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand cmd = connection.CreateCommand();

                DetachDatabase(dbName);

                cmd.CommandText = String.Format("CREATE DATABASE {0} ON (NAME = N'{0}', FILENAME = '{1}')", dbName, dbFileName);
                cmd.ExecuteNonQuery();
                cmd.CommandText = "USE Lines";
                cmd.ExecuteNonQuery();

                counter = 0;
            }

            if (File.Exists(dbFileName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool DetachDatabase(string dbName)
        {
            try
            {
                string connectionString = String.Format(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=SSPI;");
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = String.Format("exec sp_detach_db '{0}'", dbName);
                    cmd.ExecuteNonQuery();

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}