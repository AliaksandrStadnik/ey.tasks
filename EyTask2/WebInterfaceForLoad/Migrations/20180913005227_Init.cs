﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace WebInterfaceForLoad.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BalanceAccount",
                columns: table => new
                {
                    BalanceAccountId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BalanceAccountNumber = table.Column<int>(nullable: false),
                    Credit = table.Column<decimal>(nullable: false),
                    Debit = table.Column<decimal>(nullable: false),
                    IncomingBalanceActive = table.Column<decimal>(nullable: false),
                    IncomingBalancePassive = table.Column<decimal>(nullable: false),
                    OutcomingBalanceActive = table.Column<decimal>(nullable: false),
                    OutcomingBalancePassive = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BalanceAccount", x => x.BalanceAccountId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BalanceAccount");
        }
    }
}
