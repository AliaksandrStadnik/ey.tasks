﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebInterfaceForLoad.Data;
using WebInterfaceForLoad.Models;
using WebInterfaceForLoad.Utilities;

namespace WebInterfaceForLoad.Controllers
{
    public class HomeController : Controller
    {
        BalanceAccountDbContext _context;
        IHostingEnvironment _appEnvironment;
        ParseFileService _parseFileService;

        public HomeController(BalanceAccountDbContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
            _parseFileService = new ParseFileService(_context);
        }

        public IActionResult Index()
        {
            return View();
        }
        
        public IActionResult BalanceAccount()
        {
            return View(_parseFileService.GetAllAccounts());
        }

        [HttpPost]
        public async Task<IActionResult> AddFile(IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                 await _parseFileService.ParseFile(uploadedFile.OpenReadStream());
            }

            return RedirectToAction("Index");
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
