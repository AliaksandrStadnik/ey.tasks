﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebInterfaceForLoad.Models;

namespace WebInterfaceForLoad.Data
{
    public class BalanceAccountDbContext : DbContext
    {
        public BalanceAccountDbContext(DbContextOptions<BalanceAccountDbContext> options)
    : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<BalanceAccount> BalanceAccount { get; set; }


    }
}
