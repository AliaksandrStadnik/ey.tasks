﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClosedXML;
using System.Data.Common;
using System.Threading.Tasks;
using WebInterfaceForLoad.Models;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using System.IO;
using WebInterfaceForLoad.Data;

namespace WebInterfaceForLoad.Utilities
{
    public class ParseFileService
    {
        private List<BalanceAccount> ListOfBalanceAccount { get; set; }

        private BalanceAccountDbContext _context;

        public ParseFileService(BalanceAccountDbContext context)
        {
            _context = context;
            ListOfBalanceAccount = new List<BalanceAccount>();
        }

        public async Task ParseFile(Stream file)
        {
            var workbook = new XLWorkbook(file);
            var worksheet = workbook.Worksheet(1);
            var rows = worksheet.RangeUsed().RowsUsed();
            bool flagok = false;
            foreach (var row in rows)
            {
                if (row.Cell(1).CachedValue.ToString().Contains("КЛАСС") && !row.Cell(1).CachedValue.ToString().Contains("ПО КЛАССУ"))
                {
                    flagok = true;
                }
                else if (flagok && !row.Cell(1).CachedValue.ToString().Contains("ПО КЛАССУ") && !row.Cell(1).CachedValue.ToString().Contains("БАЛАНС"))
                {
                    ListOfBalanceAccount.Add(new BalanceAccount
                    {
                        BalanceAccountNumber = Int32.Parse(row.Cell(1).CachedValue.ToString()),
                        IncomingBalanceActive = Decimal.Parse(row.Cell(2).CachedValue.ToString()),
                        IncomingBalancePassive = Decimal.Parse(row.Cell(3).CachedValue.ToString()),
                        Debit = Decimal.Parse(row.Cell(4).CachedValue.ToString()),
                        Credit = Decimal.Parse(row.Cell(5).CachedValue.ToString()),
                        OutcomingBalanceActive = Decimal.Parse(row.Cell(6).CachedValue.ToString()),
                        OutcomingBalancePassive = Decimal.Parse(row.Cell(7).CachedValue.ToString())

                    });
                }

            }
            for (int i = 0; i < ListOfBalanceAccount.Count; i++)
            {
                _context.BalanceAccount.Add(ListOfBalanceAccount[i]);
            }
            await _context.SaveChangesAsync();

        }

        public List<BalanceAccount> GetAllAccounts()
        {
            return _context.BalanceAccount.ToList<BalanceAccount>();
        }
    }
}
