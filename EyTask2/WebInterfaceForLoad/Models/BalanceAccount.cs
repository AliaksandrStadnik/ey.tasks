﻿namespace WebInterfaceForLoad.Models
{
    public class BalanceAccount
    {
        public int BalanceAccountId { get; set; }

        public int BalanceAccountNumber { get; set; }

        public decimal IncomingBalancePassive { get; set; }

        public decimal IncomingBalanceActive { get; set; }

        public decimal OutcomingBalancePassive { get; set; }

        public decimal OutcomingBalanceActive { get; set; }

        public decimal Debit { get; set; }

        public decimal Credit { get; set; }
    }
}
